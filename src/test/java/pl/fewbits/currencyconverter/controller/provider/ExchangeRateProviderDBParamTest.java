package pl.fewbits.currencyconverter.controller.provider;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.fewbits.currencyconverter.domain.Currency;
import pl.fewbits.currencyconverter.domain.ExchangeRate;
import pl.fewbits.currencyconverter.provider.ExchangeRateProviderDB;
import pl.fewbits.currencyconverter.repository.ExchangeRateRepository;

@RunWith(Parameterized.class)
public class ExchangeRateProviderDBParamTest {

  private static final double PLN_USD = 2.75;

  private static final double EUR_PLN = 42.83;

  private static final double GBP_USD = 12.98;

  private static final double USD_EUR = 8.48;

  private static final double PLN_GBP = 2.12;

  @Parameterized.Parameters(name = "{index}: 1 {0} should be equal to {2} {1}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][]{
        {Currency.PLN, Currency.USD, PLN_USD},
        {Currency.EUR, Currency.PLN, EUR_PLN},
        {Currency.GBP, Currency.USD, GBP_USD},
        {Currency.USD, Currency.EUR, USD_EUR},
        {Currency.PLN, Currency.GBP, PLN_GBP},
    });
  }

  private String fromCurrency;

  private String toCurrency;

  private double expectedRate;

  private ExchangeRateRepository rateRepository;

  public ExchangeRateProviderDBParamTest(String fromCurrency, String toCurrency, double expectedRate) {
    this.fromCurrency = fromCurrency;
    this.toCurrency = toCurrency;
    this.expectedRate = expectedRate;

    rateRepository = mock(ExchangeRateRepository.class);

    when(rateRepository.findExchangeRateByFromCurrencyAndToCurrency(Currency.PLN, Currency.USD)).thenReturn(new ExchangeRate(0, Currency.PLN, Currency.USD, PLN_USD));
    when(rateRepository.findExchangeRateByFromCurrencyAndToCurrency(Currency.EUR, Currency.PLN)).thenReturn(new ExchangeRate(1, Currency.EUR, Currency.PLN, EUR_PLN));
    when(rateRepository.findExchangeRateByFromCurrencyAndToCurrency(Currency.GBP, Currency.USD)).thenReturn(new ExchangeRate(2, Currency.GBP, Currency.USD, GBP_USD));
    when(rateRepository.findExchangeRateByFromCurrencyAndToCurrency(Currency.USD, Currency.EUR)).thenReturn(new ExchangeRate(3, Currency.USD, Currency.EUR, USD_EUR));
    when(rateRepository.findExchangeRateByFromCurrencyAndToCurrency(Currency.PLN, Currency.GBP)).thenReturn(new ExchangeRate(4, Currency.PLN, Currency.GBP, PLN_GBP));
  }

  @Test
  public void GivenFromToCurrencies_WhenProvideRate_ReturnExpectedRate() {
    ExchangeRateProviderDB exchangeRateProviderDB = new ExchangeRateProviderDB(rateRepository);
    double rate = exchangeRateProviderDB.getRate(fromCurrency, toCurrency);
    assertThat(rate).isEqualTo(expectedRate);
  }
}
