package pl.fewbits.currencyconverter.controller.provider;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import pl.fewbits.currencyconverter.controller.exception.ExchangeRateNotFoundException;
import pl.fewbits.currencyconverter.provider.ExchangeRateProviderDB;
import pl.fewbits.currencyconverter.repository.ExchangeRateRepository;

public class ExchangeRateProviderDBTest {

  @Test(expected = ExchangeRateNotFoundException.class)
  public void GivenFromToCurrencies_WhenProvideRate_ReturnExpectedRate() {
    ExchangeRateRepository rateRepository = mock(ExchangeRateRepository.class);
    when(rateRepository.findExchangeRateByFromCurrencyAndToCurrency("PLN", "USD")).thenReturn(null);
    ExchangeRateProviderDB exchangeRateProviderDB = new ExchangeRateProviderDB(rateRepository);
    exchangeRateProviderDB.getRate("PLN", "USD");
  }
}
