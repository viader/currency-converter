package pl.fewbits.currencyconverter.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import org.junit.Test;
import pl.fewbits.currencyconverter.domain.Amount;
import pl.fewbits.currencyconverter.domain.Currency;
import pl.fewbits.currencyconverter.provider.ExchangeRateProvider;

public class CurrencyConverterControllerTest {

  @Test
  public void GivenZeroPLN_WhenConvertToUSD_ShouldReturnZeroUSD() {
    ExchangeRateProvider exchangeRateProvider = mock(ExchangeRateProvider.class);
    CurrencyConverterController converter = new CurrencyConverterController(exchangeRateProvider);
    Amount plnAmount = new Amount(BigDecimal.ZERO, Currency.PLN);
    Amount convertedAmount = converter.convert(plnAmount, Currency.USD);

    assertThat(convertedAmount).isEqualTo(new Amount(BigDecimal.ZERO, Currency.USD));
  }
}
