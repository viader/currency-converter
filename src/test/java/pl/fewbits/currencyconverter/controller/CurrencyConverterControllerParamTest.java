package pl.fewbits.currencyconverter.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.fewbits.currencyconverter.controller.request.ConvertAmountRequest;
import pl.fewbits.currencyconverter.controller.response.ConvertAmountResponse;
import pl.fewbits.currencyconverter.domain.Amount;
import pl.fewbits.currencyconverter.domain.Currency;
import pl.fewbits.currencyconverter.provider.ExchangeRateProvider;

@RunWith(Parameterized.class)
public class CurrencyConverterControllerParamTest {

  @Parameterized.Parameters(name = "{index}: inputAmount({0} {1}) convert to {2} expected({3} {4}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][]{
        {10, Currency.PLN, Currency.USD, 2.75, Currency.USD},
        {10, Currency.EUR, Currency.PLN, 42.83, Currency.PLN},
        {10, Currency.GBP, Currency.USD, 12.98, Currency.USD},
        {10, Currency.USD, Currency.EUR, 8.48, Currency.EUR},
        {10, Currency.PLN, Currency.GBP, 2.12, Currency.GBP},
    });
  }

  private Amount inputAmount;

  private String convertCurrency;

  private Amount expectedAmount;

  private ExchangeRateProvider exchangeRateProvider;

  public CurrencyConverterControllerParamTest(double inputValue,
                                              String inputCurrency,
                                              String convertCurrency,
                                              double expectedValue,
                                              String expectedCurrency) {
    this.inputAmount = new Amount(BigDecimal.valueOf(inputValue), inputCurrency);
    this.expectedAmount = new Amount(BigDecimal.valueOf(expectedValue), expectedCurrency);
    this.convertCurrency = convertCurrency;
    this.exchangeRateProvider = mock(ExchangeRateProvider.class);

    when(exchangeRateProvider.getRate(Currency.PLN, Currency.USD)).thenReturn(0.275283);
    when(exchangeRateProvider.getRate(Currency.USD, Currency.PLN)).thenReturn(3.63263);

    when(exchangeRateProvider.getRate(Currency.PLN, Currency.EUR)).thenReturn(0.233463);
    when(exchangeRateProvider.getRate(Currency.EUR, Currency.PLN)).thenReturn(4.28334);

    when(exchangeRateProvider.getRate(Currency.PLN, Currency.GBP)).thenReturn(0.212261);
    when(exchangeRateProvider.getRate(Currency.GBP, Currency.PLN)).thenReturn(4.71118);

    when(exchangeRateProvider.getRate(Currency.EUR, Currency.USD)).thenReturn(1.17933);
    when(exchangeRateProvider.getRate(Currency.USD, Currency.EUR)).thenReturn(0.847938);

    when(exchangeRateProvider.getRate(Currency.EUR, Currency.GBP)).thenReturn(0.908994);
    when(exchangeRateProvider.getRate(Currency.GBP, Currency.EUR)).thenReturn(1.10012);

    when(exchangeRateProvider.getRate(Currency.USD, Currency.GBP)).thenReturn(0.770709);
    when(exchangeRateProvider.getRate(Currency.GBP, Currency.USD)).thenReturn(1.29751);
  }

  @Test
  public void GivenInputAmount_WhenConvert_ShouldReturnExpectedAmount() {
    CurrencyConverterController converter = new CurrencyConverterController(exchangeRateProvider);
    Amount convertedAmount = converter.convert(inputAmount, convertCurrency);

    assertThat(convertedAmount).isEqualTo(expectedAmount);
  }

  @Test
  public void GivenInputRequest_WhenConvert_ShouldReturnExpectedResponse() {
    CurrencyConverterController converter = new CurrencyConverterController(exchangeRateProvider);
    ConvertAmountRequest request = new ConvertAmountRequest(inputAmount.getValue().toString(), inputAmount.getCurrency(), convertCurrency);
    ConvertAmountResponse response = converter.convert(request);
    assertThat(response.getValue()).isEqualTo(expectedAmount.getValue().toString());
    assertThat(response.getCurrency()).isEqualTo(expectedAmount.getCurrency());
  }
}
