package pl.fewbits.currencyconverter.controller.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.Getter;
import pl.fewbits.currencyconverter.domain.Amount;

@Getter
public class ConvertAmountRequest {

  private Amount amount;

  private String toCurrency;

  @JsonCreator
  public ConvertAmountRequest(@JsonProperty("fromValue") String fromValue,
                              @JsonProperty("fromCurrency") String fromCurrency,
                              @JsonProperty("toCurrency") String toCurrency) {
    amount = new Amount(new BigDecimal(fromValue), fromCurrency);
    this.toCurrency = toCurrency;
  }
}
