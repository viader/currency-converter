package pl.fewbits.currencyconverter.controller.response;

import lombok.Data;
import lombok.NonNull;

@Data
public class ConvertAmountResponse {

  @NonNull
  private String value;

  @NonNull
  private String currency;
}
