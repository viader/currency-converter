package pl.fewbits.currencyconverter.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Exchange rate between requested currencies is not found in the system!")
public class ExchangeRateNotFoundException extends RuntimeException {
}
