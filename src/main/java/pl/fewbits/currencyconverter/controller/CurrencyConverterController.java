package pl.fewbits.currencyconverter.controller;

import java.math.BigDecimal;
import javax.validation.Valid;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.fewbits.currencyconverter.controller.request.ConvertAmountRequest;
import pl.fewbits.currencyconverter.controller.response.ConvertAmountResponse;
import pl.fewbits.currencyconverter.domain.Amount;
import pl.fewbits.currencyconverter.provider.ExchangeRateProvider;

@RequiredArgsConstructor
@RestController
public class CurrencyConverterController {

  @NonNull
  @Autowired
  private ExchangeRateProvider exchangeRateProvider;

  @RequestMapping(value = "/api/convertAmount", method = RequestMethod.POST, produces = "application/json")
  @ResponseBody
  public ConvertAmountResponse convert(@NonNull @Valid @RequestBody ConvertAmountRequest request) {
    Amount convertedAmount = convert(request.getAmount(), request.getToCurrency());
    return new ConvertAmountResponse(convertedAmount.getValue().toString(), convertedAmount.getCurrency());
  }

  public Amount convert(@NonNull Amount inputAmount, @NonNull String outputCurrency) {
    double exchangeRate = exchangeRateProvider.getRate(inputAmount.getCurrency(), outputCurrency);
    BigDecimal convertedValue = inputAmount.getValue().multiply(BigDecimal.valueOf(exchangeRate));
    return new Amount(convertedValue, outputCurrency);
  }
}