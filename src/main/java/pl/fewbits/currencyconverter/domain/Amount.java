package pl.fewbits.currencyconverter.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.NonNull;
import lombok.Value;

@Value
public class Amount {

  @NonNull
  BigDecimal value;

  @NonNull
  String currency;

  public Amount(@NonNull BigDecimal value, @NonNull String currency) {
    this.value = new BigDecimal(value.toString()).setScale(2, RoundingMode.HALF_UP);
    this.currency = currency;
  }
}
