package pl.fewbits.currencyconverter.domain;

public interface Currency {

  String PLN = "PLN";
  String USD = "USD";
  String EUR = "EUR";
  String GBP = "GBP";
}
