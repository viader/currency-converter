package pl.fewbits.currencyconverter.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Entity
public class ExchangeRate {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "exchange_rate_id", unique = true, nullable = false)
  private Integer id;

  private String fromCurrency;

  private String toCurrency;

  private double rate;

  public ExchangeRate() {

  }
}
