package pl.fewbits.currencyconverter;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.fewbits.currencyconverter.domain.ExchangeRate;
import pl.fewbits.currencyconverter.repository.ExchangeRateRepository;

@SpringBootApplication
@EnableJpaRepositories
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Bean
  public CommandLineRunner demo(ExchangeRateRepository repository) {
    return (args) -> {
      repository.save(new ExchangeRate(1, "PLN", "USD", 0.275283));
      repository.save(new ExchangeRate(2, "USD", "PLN", 3.63263));

      repository.save(new ExchangeRate(3, "PLN", "EUR", 0.233463));
      repository.save(new ExchangeRate(4, "EUR", "PLN", 4.28334));

      repository.save(new ExchangeRate(5, "PLN", "GBP", 0.212261));
      repository.save(new ExchangeRate(6, "GBP", "PLN", 4.71118));

      repository.save(new ExchangeRate(7, "EUR", "USD", 1.17933));
      repository.save(new ExchangeRate(8, "USD", "EUR", 0.847938));

      repository.save(new ExchangeRate(9, "EUR", "GBP", 0.908994));
      repository.save(new ExchangeRate(10, "GBP", "EUR", 1.10012));

      repository.save(new ExchangeRate(11, "USD", "GBP", 0.770709));
      repository.save(new ExchangeRate(12, "GBP", "USD", 1.29751));
    };
  }
}