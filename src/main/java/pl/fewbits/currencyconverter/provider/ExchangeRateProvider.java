package pl.fewbits.currencyconverter.provider;

import pl.fewbits.currencyconverter.controller.exception.ExchangeRateNotFoundException;

public interface ExchangeRateProvider {

  double getRate(String fromCurrency, String toCurrency) throws ExchangeRateNotFoundException;
}
