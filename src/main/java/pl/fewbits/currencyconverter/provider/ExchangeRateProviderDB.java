package pl.fewbits.currencyconverter.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.fewbits.currencyconverter.controller.exception.ExchangeRateNotFoundException;
import pl.fewbits.currencyconverter.domain.ExchangeRate;
import pl.fewbits.currencyconverter.repository.ExchangeRateRepository;

@Component
public class ExchangeRateProviderDB implements ExchangeRateProvider {

  ExchangeRateRepository repository;

  @Autowired
  public ExchangeRateProviderDB(ExchangeRateRepository repository) {
    this.repository = repository;
  }

  @Override
  public double getRate(String fromCurrency, String toCurrency) {
    ExchangeRate exchangeRate = repository.findExchangeRateByFromCurrencyAndToCurrency(fromCurrency, toCurrency);
    if (exchangeRate == null) {
      throw new ExchangeRateNotFoundException();
    }
    return repository.findExchangeRateByFromCurrencyAndToCurrency(fromCurrency, toCurrency).getRate();
  }
}
