package pl.fewbits.currencyconverter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.fewbits.currencyconverter.domain.ExchangeRate;

@Repository
public interface ExchangeRateRepository extends CrudRepository<ExchangeRate, Integer> {

  ExchangeRate findExchangeRateByFromCurrencyAndToCurrency(String fromCurrency, String toCurrency);
}